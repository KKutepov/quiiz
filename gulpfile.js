var gulp = require('gulp');
var minifyCss = require('gulp-minify-css');
var sass = require('gulp-sass');
var notify = require('gulp-notify');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var babel = require('gulp-babel');

var paths = {
    html: ['./app/**/*.html'],
    css: ['./app/**/*.*ss'],
    script: ['./app/**/*.js'],
    fonts: ['./app/fonts/**/*']
};

// ////////////////////////////////////////////////
// HTML, CSS, JS
// ///////////////////////////////////////////////
gulp.task('mincss', function () {
    return gulp.src(paths.css)
        .pipe(sass().on('error', sass.logError))
        .pipe(minifyCss())
        .pipe(gulp.dest('dist/'))
        .pipe(reload({stream: true}));
});

gulp.task('html', function () {
    gulp.src(paths.html)
        .pipe(gulp.dest('dist/'))
        .pipe(reload({stream: true}));
});

gulp.task('fonts', function () {
    gulp.src(paths.fonts)
        .pipe(gulp.dest('dist/fonts'))
        .pipe(reload({stream: true}));
});

gulp.task('scripts', function () {
    return gulp.src(paths.script)
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(gulp.dest('dist/'))
        .pipe(reload({stream: true}));
});

// ////////////////////////////////////////////////
// Browser-Sync
// // /////////////////////////////////////////////
gulp.task('browserSync', ['mincss', 'scripts', 'html', 'fonts'], function () {
    browserSync({
        server: {
            baseDir: "./dist/"
        },
        open: true,
        notify: false
    });
});

gulp.task('watcher', function () {
    gulp.watch(paths.css, ['mincss']);
    gulp.watch(paths.script, ['scripts']);
    gulp.watch(paths.html, ['html']);
    gulp.watch(paths.fonts, ['fonts']);
});

gulp.task('default', ['watcher', 'browserSync'], function () {
    gulp.watch(paths.css, ['mincss']);
    gulp.watch(paths.script, ['scripts']);
    gulp.watch(paths.html, ['html']);
    gulp.watch(paths.fonts, ['fonts']);
});