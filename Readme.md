
#Quizz table app

## Project map
 
    ├───app                   ; <---  this is folder to developing
    │   ├───css
    │   ├───fonts
    │   │   └───roboto
    │   ├───scripts
    ├───dist                  ; <--- that is folder contains prod
    │   ├───css
    │   ├───fonts
    │   │   └───roboto
    │   └───scripts


The ``.\app\  ``   folder contains all source code. _**All project changes must be happens here**_.
    
    .\app\css\          --- contains all source css/scss rules.
    .\app\scripts\      --- contains all source JS files (using ES6). 
    .\app\index.html    --- main source HTML view page. 

The ``.\dist\  ``  folder generated automatically by Gulp after processing source code (from ``.\app\  ``   folder). This is a production folder (all files are minimized and simplified and ready to use on server). You can put content of this folder into hosting domain folder (if needed).

    .\dist\css\          --- contains all proccessed css (minified and simplified).
    .\dist\scripts\      --- contains all proccessed JS (translated to ES5). 
    .\dist\index.html    --- main HTML view page. 

The root of repo (``.\  ``) contains ``gulpfile.js`` with all gulp tasks and options. 

## How to run the app to develop on local machine?

The best way to run Quiiz Table app:

1. Install git on your machine 

2. Clone this repo to your local app folder (choose the path manually) 

    > git clone https://KKutepov@bitbucket.org/KKutepov/quiiz.git path\to\your\app\folder
    
3. Download and install latest version of Node.js from official page:https://nodejs.org/

4. Go to the app folder :

    > cd path\to\your\app\folder

5. Update all project dependencies using **npm** :

    > npm install
    
6. Run the app using **gulp** (See docs on the official page: https://gulpjs.com/):

    > gulp
    
7. Browsers must open your "localhost:3000" page.

8. Try to make changes in the code, save it and you will see that opened page in browser has been reloaded. It's live-reload magic) Enjoy this.

## Contacts

Be comfortable to send me email on `kkutepovgg@gmail.com`