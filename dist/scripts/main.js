"use strict";

$(function () {
    initialize();
});

var timeout = void 0;

function clearAllData() {
    var $columns = $("#table-container table>thead>tr");
    var $tagsContainer = $('#tags-container');
    var $tableBody = $("#table-container table>tbody");

    // Clear all elements
    $columns.empty();
    $tagsContainer.empty();
    $tableBody.empty();
    return { $columns: $columns, $tagsContainer: $tagsContainer, $tableBody: $tableBody };
}

function createColumns($columns) {
    // Create columns
    for (var i = 0; i <= quizz.columns.length; i++) {
        $columns.append($("<th></th>").html(i === 0 ? "" : quizz.columns[i - 1]));
    }
}

function createRows($tableBody) {
    // Create rows
    for (var i = 0; i < quizz.rows.length; i++) {
        var $tr = $("<tr></tr>");
        for (var j = 0; j <= quizz.columns.length; j++) {
            $tr.append(j === 0 ? $("<td></td>").html("<b>" + quizz.rows[i] + "</b>") : $("<td></td>").addClass("droppable").attr("data-row", i).attr("data-col", j - 1).html(""));
        }
        $tableBody.append($tr);
    }
}

function createTable($columns, $tableBody) {
    createColumns($columns);
    createRows($tableBody);
}

function createTags($tagsContainer) {
    // Create tags
    var tags = shuffle(quizz.tags.reduce(function (prev, curr) {
        return prev.concat(curr);
    }));
    for (var i = 0; i < tags.length; i++) {
        $tagsContainer.append(
        // TODO: maybe extract color classes to style.css?
        $("<div></div>").addClass("chip draggable black accent-3 white-text").text(tags[i]));
    }
}

function enableDragDrop() {
    // Enable touch-friendly dragging with reverting option
    $(".draggable").draggable({ revert: true });

    // Enable drop with callback
    $(".droppable").droppable({
        drop: function drop(event, ui) {
            // lock the target cell for another tags
            $(this).droppable("disable");

            // unlock the previous cell
            var prevParent = $(ui.helper[0]).parent("td");
            $(prevParent).droppable("enable");

            // put tag to the target cell
            $(ui.helper[0]).attr("style", "position: relative").appendTo(this);
        }
    });
}

function initialize() {
    $("#result-dialog-background").hide();

    var _clearAllData = clearAllData(),
        $columns = _clearAllData.$columns,
        $tagsContainer = _clearAllData.$tagsContainer,
        $tableBody = _clearAllData.$tableBody;

    createTable($columns, $tableBody);
    createTags($tagsContainer);

    enableDragDrop();

    // run 60-second timer
    restartCounter(60);
}

// restart counter logic
//      timetotal:Integer - total time (in seconds)
//      timeleft:Integer - started time value (in seconds)
var restartCounter = function restartCounter(timetotal, timeleft) {

    var $wrapper = $('#progress-background');
    var $fill = $("#progress-fill");
    var $label = $("#timeleft-label");

    if (!timeleft) timeleft = timetotal;

    // force stop the last counter
    clearTimeout(timeout);

    // calculate width of progressBar
    var calculatedWidth = timeleft * $wrapper.width() / timetotal;

    // run animation to change width of progressBar
    $fill.animate({ width: calculatedWidth }, 500);

    // update time label
    $label.html(timeleft);

    // stop if time is out
    if (timeleft > 0) {
        timeout = setTimeout(function () {
            restartCounter(timetotal, timeleft - 1);
        }, 1000);
    } else {
        finishGame();
    }
};

var finishGame = function finishGame() {
    // force stop of last timecounter
    clearTimeout(timeout);

    // compare all cells values with quizz.tags values
    var validCount = 0;
    for (var i = 0; i < quizz.tags.length; i++) {
        for (var j = 0; j < quizz.tags[i].length; j++) {
            var val = $("td[data-row=" + i + "][data-col=" + j + "]>div").html();
            var isValid = val === quizz.tags[i][j];
            validCount += isValid ? 1 : 0;
            $("td[data-row=" + i + "][data-col=" + j + "], " + "td[data-row=" + i + "][data-col=" + j + "]>div").addClass(isValid ? "green" : "red").addClass("accent-3");
        }
    }
    var totalCount = quizz.tags.length * quizz.tags[0].length;
    var level = Math.floor(validCount / totalCount * 4);
    $("#result-dialog-text").html("(" + validCount + " из " + totalCount + ")<br/>" + getRank(level));
    $("#result-dialog-background").show();

    $(".ui-droppable").droppable("disable");
    $(".ui-draggable").draggable("disable");
};

function getRank(level) {
    switch (level) {
        case 0:
            return "Вы - новичок!";
        case 1:
            return "Вы - опытный!";
        case 2:
            return "Вы - профессионал!";
        default:
            return "Вы - эксперт!";
    }
}

var shuffle = function shuffle(a) {
    var j = void 0,
        x = void 0,
        i = void 0;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
};

var quizz = {
    columns: ["0", "1", "2"], // column names
    rows: ["A", "B", "C", "D", "E"], // row names
    tags: [["A0", "A1", "A2"], // The ORDER of tags is the key to quiz
    ["B0", "B1", "B2"], ["C0", "C1", "C2"], ["D0", "D1", "D2"], ["E0", "E1", "E2"]]
};

// let quizz = {
//     columns: ["Инициация", "Планирование", "Исполнение", "Мониоринг и контроль", "Закрытие"],
//     rows: ["Управление интегшрацией проекта", "Управление содержанием проекта", "Управление сроками проекта",
//         "Управление стоимостью проекта", "Управление качеством проекта", "Управление человеческими ресурсами проекта",
//         "Управление коммуникациями проекта", "Управление рисками проекта", "Управление поставками проекта",
//         "Управление заинтересованными сторонами"],
//     tags: ["Определение заинтересованных сторон", "Количественный анализ рынков", "Сбор требований",
//         "Подтверждение содержания", "Определение бюджета", "Закрытие закупок", "Обеспечение качества"]
// };